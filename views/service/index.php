<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Services list';
?>
<div class="site-index">

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование</th>
            <th scope="col">Цена</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($services as $id => $service) : ?>
            <?php /* @var array $payment */ ?>
            <tr>
                <th scope="row"><?= $id ?></th>
                <th scope="row"><?= $service['name'] ?></th>
                <th scope="row"><?= $service['price'] ?></th>
            </tr>
        <?php endforeach ?>

        </tbody>
    </table>

</div>
