<?php

/* @var $this yii\web\View */

$this->title = 'Payments list';
?>
<div class="site-index">

    <? /* @var \app\models\Payment $payment */ ?>
    <table class="table">
        <tbody>
        <tr>
            <td>Номер платежки</td>
            <td><?= $payment->id ?></td>
        </tr>
        <tr>
            <td>ФИО пользователя</td>
            <td><?= $payment->user->name ?></td>
        </tr>
        <tr>
            <td>Дата платежки</td>
            <td><?= date('Y-m-d H:i:s', $payment->create_time) ?></td>
        </tr>
        </tbody>
    </table>

    <?php $count = 0 ?>
    <?php $amount_total = 0 ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование услуги</th>
            <th scope="col">Кол-во</th>
            <th scope="col">Цена</th>
            <th scope="col">Сумма</th>
        </tr>
        </thead>
        <tbody>
        <? /* @var \app\models\Payment $payment */ ?>
        <?php foreach ($payment->servicesUsers as $service_user) : ?>
            <? /* @var \app\models\ServicesUsers $service_user */ ?>
            <?php $count += $service_user->count ?>
            <?php $amount = $service_user->service->price * $service_user->count ?>
            <?php $amount_total += $amount ?>
            <tr>
                <td><?= $service_user->id ?></td>
                <td><?= $service_user->service->name ?></td>
                <td><?= $service_user->count ?></td>
                <td><?= $service_user->service->price ?></td>
                <td><?= $amount ?></td>
            </tr>
        <?php endforeach ?>
        <tr>
            <td colspan="2"></td>
            <td><?= $count ?></td>
            <td></td>
            <td><?= $amount_total ?></td>
        </tr>
        </tbody>
    </table>

</div>
