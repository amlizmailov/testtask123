<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Payments list';
?>
<div class="site-index">

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Дата формирования</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($payments as $id => $payment) : ?>
            <?php /* @var array $payment */ ?>
            <tr>
                <th scope="row"><?= $id ?></th>
                <th scope="row"><?= date('Y-m-d H:i:s', $payment['create_time']) ?></th>
                <th scope="row"><?= Html::a('Подробнее',
                        Url::to(['payment/view/' . $id])) ?></th>
                <th scope="row"><?= Html::a('Версия для печати',
                        Url::to(['payment/html/' . $id])) ?></th>
            </tr>
        <?php endforeach ?>

        </tbody>
    </table>

</div>
