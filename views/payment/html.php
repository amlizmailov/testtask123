<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Payments list';
?>
<div class="site-index">

    <? /* @var \app\models\Users $customer */ ?>
    <table class="table">
        <tbody>
        <tr>
            <td>Продавец</td>
            <td><?= $customer->name ?></td>
        </tr>
        <tr>
            <td>Адрес</td>
            <td><?= $customer->adress ?></td>
        </tr>
        <tr>
            <td>ИНН</td>
            <td><?= $customer->inn ?></td>
        </tr>
        <tr>
            <td>Расчетный счет</td>
            <td><?= $customer->rschet ?></td>
        </tr>
        <tr>
            <td>Кор. счет</td>
            <td><?= $customer->korschet ?></td>
        </tr>
        <tr>
            <td>БИК</td>
            <td><?= $customer->bik ?></td>
        </tr>
        <tr>
            <td>Банк</td>
            <td><?= $customer->bank ?></td>
        </tr>
        </tbody>
    </table>

    <p></p>

    <? /* @var \app\models\Payment $payment */ ?>
    <? /* @var \app\models\Users $buyer */ ?>
    <?php $buyer = $payment->user ?>
    <table class="table">
        <tbody>
        <tr>
            <td>Покупатель</td>
            <td><?= $buyer->name ?></td>
        </tr>
        <tr>
            <td>Адрес</td>
            <td><?= $buyer->adress ?></td>
        </tr>
        <tr>
            <td>ИНН</td>
            <td><?= $buyer->inn ?></td>
        </tr>
        <tr>
            <td>КПП</td>
            <td><?= $buyer->kpp ?></td>
        </tr>
        <tr>
            <td>Расчетный счет</td>
            <td><?= $buyer->rschet ?></td>
        </tr>
        <tr>
            <td>Кор. счет</td>
            <td><?= $buyer->korschet ?></td>
        </tr>
        <tr>
            <td>БИК</td>
            <td><?= $buyer->bik ?></td>
        </tr>
        <tr>
            <td>Банк</td>
            <td><?= $buyer->bank ?></td>
        </tr>
        </tbody>
    </table>

    <p></p>

    <h1 class="text-center">№ <?= $payment->id ?> от <?= date('d.m.Y', $payment->create_time) ?></h1>
    <?php $count = 0 ?>
    <?php $amount_total = 0 ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование услуги</th>
            <th scope="col">Кол-во</th>
            <th scope="col">Цена</th>
            <th scope="col">Сумма</th>
        </tr>
        </thead>
        <tbody>
        <? /* @var \app\models\Payment $payment */ ?>
        <?php foreach ($payment->servicesUsers as $service_user) : ?>
            <? /* @var \app\models\ServicesUsers $service_user */ ?>
            <?php $count += $service_user->count ?>
            <?php $amount = $service_user->service->price * $service_user->count ?>
            <?php $amount_total += $amount ?>
            <tr>
                <td><?= $service_user->id ?></td>
                <td><?= $service_user->service->name ?></td>
                <td><?= $service_user->count ?></td>
                <td><?= $service_user->service->price ?></td>
                <td><?= $amount ?></td>
            </tr>
        <?php endforeach ?>
        <tr>
            <td colspan="2"></td>
            <td><?= $count ?></td>
            <td></td>
            <td><?= $amount_total ?></td>
        </tr>
        </tbody>
    </table>

    <p></p>

    <table class="table">
        <tbody>
        <tr>
            <td colspan="2"></td>
            <td>Индивидуальный предприниматель</td>
            <td>_______________</td>
            <td colspan="2"></td>
            <td>Подпись</td>
            <td>_______________</td>
        </tr>
        </tbody>
    </table>

</div>
