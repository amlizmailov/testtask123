<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Payment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site">
    
    <div class="container">
        <?php if (!empty($errors)) : ?>
            <?php foreach ($errors as $error): ?>
                <p><?php echo $error ?></p>
            <?php endforeach; ?>
        <?php endif ?>

        <?php if (!empty($success) && $success == 1) : ?>
            <p>Ваши данные успешно сохранены! Платежка доступна по <?= Html::a('адресу',
                    Url::to(['payment/html/' . $payment->id])) ?></p>
        <?php endif ?>
    </div>

    <div class="container">
        <?php $form = ActiveForm::begin() ?>

        <?php /* @var \app\models\Users $user */ ?>
        <?= $form->field($user, 'name')->textInput(['class' => 'form-control']) ?>
        <?= $form->field($user, 'adress')->textarea(['class' => 'form-control']) ?>
        <?= $form->field($user, 'inn')->textInput(['class' => 'form-control']) ?>
        <?= $form->field($user, 'kpp')->textInput(['class' => 'form-control']) ?>
        <?= $form->field($user, 'rschet')->textInput(['class' => 'form-control']) ?>
        <?= $form->field($user, 'korschet')->textInput(['class' => 'form-control']) ?>
        <?= $form->field($user, 'bik')->textInput(['class' => 'form-control']) ?>
        <?= $form->field($user, 'bank')->textInput(['class' => 'form-control']) ?>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Услуга</th>
                <th scope="col">Количество</th>
            </tr>
            </thead>
            <tbody id="tbody1">
            <tr>
                <td><?= Html::dropDownList('services_users[service_id][]', null, $services,
                        ['class' => 'form-control']) ?></td>
                <td><?= Html::textInput('services_users[count][]', '', ['class' => 'form-control']) ?></td>
            </tr>
            </tbody>
            <tbody id="tbody2">
            <tr>
                <td colspan="2">
                    <?= Html::button('Добавить услугу', ['class' => 'btn btn-primary', 'id' => 'addService']) ?>
                </td>
            </tr>
            </tbody>
        </table>

        <?= Html::submitButton('Сформировать счет на оплату', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end() ?>

    </div>
</div>