<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use app\models\helpers\ServiceHelper;
use app\models\helpers\PaymentHelper;

use app\models\Users;
use app\models\Services;
use app\models\Payment;
use app\models\ServicesUsers;


class PaymentController extends Controller
{
    const CUSTOMER_USER_ID = 1;

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['payments' => PaymentHelper::toArray(Payment::getList())]);
    }

    /**
     * просмотр конкретной платежки
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        $payment = Payment::getById($id);
        if (!$payment) {
            throw new NotFoundHttpException('Payment not found');
        }

        return $this->render('view', ['payment' => $payment]);
    }

    /**
     * версия для печати
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionHtml()
    {
        $id = Yii::$app->request->get('id');
        $payment = Payment::getById($id);
        if (!$payment) {
            throw new NotFoundHttpException('Payment not found');
        }

        $customer = Users::getById(self::CUSTOMER_USER_ID);

        return $this->renderPartial('html', ['payment' => $payment, 'customer' => $customer]);
    }

    /**
     * Форма формирования счета
     *
     * @return string
     */
    public function actionPay()
    {
        $post = Yii::$app->request->post();

        $user = new Users();
        $services = ServiceHelper::toArray(Services::getActiveServices());

        $data = [
            'user' => $user,
            'services' => ServiceHelper::formatSelect(Services::getActiveServices()),
        ];

        if ($post && $user->load($post, 'Users')) {
            $user->role = Users::ROLE_BUYER;
            if (!$user->save()) {
                $data['errors'] = $user->getErrors();
            }

            if ($user) {
                // сохраняем платежку
                $payment = new Payment();
                $payment->user_id = $user->id;
                if (!$payment->save()) {
                    $data['errors'] = $payment->getErrors();
                }
                $data['payment'] = $payment;

                // сохраняем данные об услугах из платежки
                foreach ($post['services_users']['service_id'] as $key => $service_id) {
                    $count = isset($post['services_users']['count']) ? $post['services_users']['count'][$key] : 0;
                    $price = isset($services[$service_id]) ? $services[$service_id]['price'] : 0;
                    $amount = $count * $price;

                    $services_users = new ServicesUsers();
                    $services_users->user_id = $user->id;
                    $services_users->service_id = $service_id;
                    $services_users->payment_id = $payment->id;
                    $services_users->count = $count;
                    $services_users->amount = $amount;

                    if (!$services_users->save()) {
                        $data['errors'] = $payment->getErrors();
                    }
                }

                $data['success'] = 1;
            }
        }

        return $this->render('pay', $data);
    }

}
