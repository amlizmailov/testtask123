<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use app\models\helpers\ServiceHelper;
use app\models\helpers\PaymentHelper;

use app\models\Users;
use app\models\Services;
use app\models\Payment;
use app\models\ServicesUsers;


class ServiceController extends Controller
{
    /**
     * список услуги
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['services' => ServiceHelper::toArray(Services::getList())]);
    }

}