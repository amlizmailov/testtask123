<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $create_time
 * @property Users $user
 * @property ServicesUsers $servicesUsers
 */
class Payment extends ActiveRecord
{
    private static $table_name = "payment";

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * return table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return static::$table_name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Имя пользователя',
            'create_time' => 'Время создания платежа',
        ];
    }

    /**
     * вернуть платежку по id
     *
     * @param int $id
     * @return array|null|ActiveRecord
     */
    public static function getById(int $id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    /**
     * возвращает список всех платежок
     *
     * @return array|ActiveRecord[]
     */
    public static function getList()
    {
        return self::find()->all();
    }

    /**
     * получить инфо о пользователе
     *
     * @return yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * получить сервисы-юзеры
     *
     * @return yii\db\ActiveQuery
     */
    public function getServicesUsers()
    {
        return $this->hasMany(ServicesUsers::class, ['payment_id' => 'id']);
    }
}