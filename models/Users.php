<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property integer $kpp
 * @property integer $inn
 * @property string $adress
 * @property integer $rschet
 * @property integer $korschet
 * @property integer $bik
 * @property integer $create_time
 * @property string $bank - наименование банка, в котором обслуживается пользователь
 * @property string $role  - роль пользователя
 */
class Users extends ActiveRecord
{
    const ROLE_CUSTOMER = "customer";
    const ROLE_BUYER = "buyer";

    private static $table_name = "users";

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['name', 'inn', 'rschet', 'korschet', 'bik'], 'required'],
            [['inn', 'rschet', 'korschet', 'bik', 'kpp'], 'integer'],
            [['name', 'adress', 'bank', 'role'], 'string'],
            [['role'], 'in', 'range' => [static::ROLE_BUYER, static::ROLE_CUSTOMER]],
        ];
    }

    /**
     * return table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return static::$table_name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя пользователя',
            'adress' => 'Адрес',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'rschet' => 'Расчетный счет',
            'korschet' => 'Корреспондентский счет',
            'bik' => 'БИК',
            'bank' => 'Банк, в котором обслуживается пользователь',
            'create_time' => 'Время регистрации',
            'role' => 'Роль'
        ];
    }

    /**
     * вернуть пользователя по id
     *
     * @param int $id
     * @return array|null|ActiveRecord
     */
    public static function getById(int $id)
    {
        return self::find()->where(['id' => $id])->one();
    }

}