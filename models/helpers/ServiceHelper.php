<?php

namespace app\models\helpers;

use app\models\Services;

class ServiceHelper
{
    /**
     * преобразует массив ActiveRecord в массив
     *
     * @param array $services
     * @return array
     */
    public static function toArray(array $services)
    {
        $result = [];
        foreach ($services as $service) {
            /* @var Services $service */
            $result[$service->id] = ['name' => $service->name, 'price' => $service->price];
        }
        return $result;
    }

    /**
     * преобразует массив ActiveRecord в обычный массив
     *
     * @param array $services
     * @return array
     */
    public static function formatSelect(array $services)
    {
        $result = [];
        foreach ($services as $service) {
            /* @var Services $service */
            $result[$service->id] = $service->name;
        }
        return $result;
    }
}