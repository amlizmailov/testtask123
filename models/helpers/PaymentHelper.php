<?php

namespace app\models\helpers;

use app\models\Payment;

class PaymentHelper
{
    /**
     * преобразует массив ActiveRecord в обычный массив
     *
     * @param array $payments
     * @return array
     */
    public static function toArray(array $payments)
    {
        $result = [];
        foreach ($payments as $payment) {
            /* @var Payment $payment */
            $result[$payment->id] = ['user' => $payment->user->name, 'create_time' => $payment->create_time];
        }
        return $result;
    }
}