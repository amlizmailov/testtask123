<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $service_id
 * @property integer $count
 * @property double $amount
 * @property integer $payment_id
 * @property Users $user
 * @property Services $service
 * @property Payment $payment
 */
class ServicesUsers extends ActiveRecord
{
    private static $table_name = "services_users";

    public function rules()
    {
        return [
            [['user_id', 'service_id', 'count', 'amount', 'payment_id'], 'required'],
            [['user_id', 'service_id', 'count', 'payment_id'], 'integer'],
            [['amount'], 'double'],
        ];
    }

    /**
     * return table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return static::$table_name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'service_id' => 'ID услуги',
            'payment_id' => 'ID платежки',
            'count' => 'Количество',
            'amount' => 'Сумма',
        ];
    }

    /**
     * возвращает список всех услуг предоставленных пользовелям
     *
     * @param array $filter
     * @return array|ActiveRecord[]
     */
    public static function getList(array $filter = [])
    {
        $query = self::find();

        if ($filter['user_id']) {
            $query->andWhere(['user_id' => $filter['user_id']]);
        }
        if ($filter['service_id']) {
            $query->andWhere(['service_id' => $filter['service_id']]);
        }
        if ($filter['payment_id']) {
            $query->andWhere(['payment_id' => $filter['payment_id']]);
        }

        return $query->all();
    }

    /**
     * получить пользователя
     *
     * @return yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * получить услугу
     *
     * @return yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::class, ['id' => 'service_id']);
    }

    /**
     * получить платежку
     *
     * @return yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'payment_id']);
    }

}