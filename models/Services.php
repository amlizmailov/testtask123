<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property string $name
 * @property double $price
 * @property bool $active
 */
class Services extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NO_ACTIVE = 0;

    private static $table_name = "services";

    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['price'], 'double'],
            [['name'], 'string'],
        ];
    }

    /**
     * return table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return static::$table_name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование услуги',
            'price' => 'Цена',
            'active' => 'Активна',
        ];
    }

    /**
     * возвращает список всех услуг
     *
     * @return array|ActiveRecord[]
     */
    public static function getList()
    {
        return static::getListQuery()->all();
    }

    /**
     * возвращает список активных услуг
     *
     * @return array|ActiveRecord[]
     */
    public static function getActiveServices()
    {
        return static::getListQuery()->where(['active' => self::STATUS_ACTIVE])->all();
    }

    /**
     * Общая часть запроса
     *
     * @return yii\db\ActiveQuery
     */
    public static function getListQuery()
    {
        return self::find();
    }

}