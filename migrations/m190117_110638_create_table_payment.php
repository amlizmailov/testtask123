<?php

use yii\db\Migration;

/**
 * Class m190117_110638_create_table_payment
 */
class m190117_110638_create_table_payment extends Migration
{
    public $table_name = "payment";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(10)->unsigned(),
            'user_id' => $this->integer(11)->unsigned(),
            'create_time' => $this->integer(11)->unsigned()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }
}
