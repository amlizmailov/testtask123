<?php

use yii\db\Migration;
use app\models\Users;

/**
 * Class m190116_192204_create_table_users
 */
class m190116_192204_create_table_users extends Migration
{
    public $table_name = "users";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(10)->unsigned(),
            'name' => $this->string(100),
            'adress' => $this->string(100),
            'inn' => $this->integer(10)->unsigned(),
            'kpp' => $this->integer(10)->unsigned(),
            'rschet' => $this->bigInteger(20)->unsigned(),
            'korschet' => $this->bigInteger(20)->unsigned(),
            'bik' => $this->integer(9)->unsigned(),
            'bank' => $this->string(100),
            'create_time' => $this->integer(11)->unsigned(),
            'role' => $this->string(20)
        ], $tableOptions);

        // вставляем данные нашей организации
        $this->batchInsert($this->table_name, [
            'name',
            'adress',
            'inn',
            'kpp',
            'rschet',
            'korschet',
            'bik',
            'bank',
            'create_time',
            'role'
        ], [
            [
                'Индивидуальный предприниматель Иванов Сергей Петрович',
                'Санкт-Петербург, ул. Садовая, д.2, корп.2, оф. 22',
                '1234567890',
                '',
                '12345678901234567890',
                '11223344556677889900',
                '123456789',
                'ОАО "Мой Банк" г.Санкт-Петербург',
                mktime(),
                Users::ROLE_CUSTOMER
            ]
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }
}
