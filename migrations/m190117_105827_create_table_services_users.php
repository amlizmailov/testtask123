<?php

use yii\db\Migration;

/**
 * Class m190117_105827_create_table_services_users
 */
class m190117_105827_create_table_services_users extends Migration
{
    public $table_name = "services_users";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(10)->unsigned(),
            'user_id' => $this->integer(11)->unsigned(),
            'service_id' => $this->integer(11)->unsigned(),
            'count' => $this->integer(11)->unsigned(),
            'amount' => $this->decimal(10, 2)->unsigned(),
            'payment_id' => $this->integer(11)->unsigned()
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }

}