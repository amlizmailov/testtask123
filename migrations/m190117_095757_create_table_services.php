<?php

use yii\db\Migration;

/**
 * Class m190117_095757_create_table_services
 */
class m190117_095757_create_table_services extends Migration
{
    public $table_name = "services";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(10)->unsigned(),
            'name' => $this->string(100),
            'price' => $this->decimal(10, 2)->unsigned(),
            'active' => $this->boolean()->defaultValue(1),
        ], $tableOptions);

        // вставляем данные нашей организации
        $this->batchInsert($this->table_name, [
            'name',
            'price',
            'active'
        ], [
            [
                'Оказание услуг по обслуживанию компьютерной техники',
                '100000.00',
                1
            ],
            [
                'Консультация',
                '10000.00',
                1
            ],
            [
                'Изготовление наружной рекламы',
                '10000.00',
                1
            ],
            [
                'Установка спец. ПО',
                '10000.00',
                0
            ]
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }

}